require 'spec_helper'

describe Device do

  before do
    @device=FactoryGirl.build(:device)
    sensor=FactoryGirl.build(:sensor)
    sensor.device=@device
    @device.sensors << sensor

    sensor.save
    @device.save
  end

  subject{ @device}
  
  it{ should be_valid }
  
  describe "should respond to" do
    it { should respond_to :name}
    it { should respond_to :lat}
    it { should respond_to :long}
    
    it { should respond_to :sensors}
  end

  it "should not have an empty name" do
    @device.name=""
    should_not be_valid
  end

  it "should have at least one sensor" do
    @device.sensors.clear

    should_not be_valid
  end
  
  it "should not have duplicate devices (same name)" do
    other_device=@device.dup
    sensor=FactoryGirl.build(:sensor)
    other_device.sensors << sensor
    other_device.name.upcase!

    other_device.should_not be_valid
  end
  
  it "destroying device should remove all his associations" do

    alert=FactoryGirl.build(:alert)
    @device.sensors.first.alert=alert
    @device.save
    alert.sensor=@device.sensors.first
    alert.save
    
    models=[Device, Sensor, Alert]

    @device.destroy

    models.each do |model|
      model.count.should == 0 
    end
    
  end

  
  describe "coordinates should have a valid format," do
    
    it "without coordinates" do
      @device.lat=nil
      @device.long=nil

      should_not be_valid
    end

    it "with just one valid coordinate" do
      @device.long=nil
      
      should_not be_valid
    end

    it "latitude should be in a valid range" do
      @device.lat=200
      
      should_not be_valid
    end

    it "longitude should be in a valid range" do
      @device.long=200
      
      should_not be_valid
    end

  end
  
  
end


