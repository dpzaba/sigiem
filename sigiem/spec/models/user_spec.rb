require 'spec_helper'

describe User do

  before do
    @user =  FactoryGirl.create(:user)
  end

  subject{ @user }

  describe "should respond to" do
    it{ should respond_to :name}
    it{ should respond_to :email}
    it{ should respond_to :role}

    it{ should respond_to :password}
    it{ should respond_to :password_confirmation}
    it{ should respond_to :password_digest}

    it{ should respond_to :session_token}

    it{ should respond_to :authenticate}
  end

  it{ should be_valid}

  describe "role: " do
    it "should have user role by default" do
      @user.role.should == :user
    end

    let(:roles){
      roles=[:user, :admin, :super_admin]
    }
    
    it "should be user,admin or super_admin" do
      roles.should include @user.role
    end

    it "not should be other role" do
      @user.role=:root
      roles.should_not include @user.role
    end
  end
  
  describe "name :" do
    it "is too long" do
      @user.name="d"*31
      should_not be_valid
    end

    it "is too short" do
      @user.name="d"*2
      should_not be_valid
    end
    
    it "is empty" do
      @user.name=""
      should_not be_valid
    end

    it "is blank" do
      @user.name=" "
      should_not be_valid
    end
  end

  describe "password :" do
    it "is blank" do
      @user.password=@user.password_confirmation=""

      should_not be_valid
    end

    it "is too short" do
      @user.password = @user.password_confirmation = "a"*3

      should_not be_valid
    end

    it "doesn't match with confirmation" do
      @user.password_confirmation="not equals"

      should_not be_valid
    end

    it "password_confirmation is nil" do
      @user.password_confirmation=nil

      should_not be_valid
    end
    
  end

  
  describe "email :" do

    it "is valid" do
      emails=%w[david@dpzaba.com second2@user.com two@david.org valid@david.com david@dpzaba.com.es]

      emails.each do |valid_email|
        @user.email=valid_email
        should be_valid
      end
    end


    it "is invalid" do
      emails=%w[second2user.com two@with@david.org invalid@david,com invalid_symbols@david+dpzaba.org]

      emails.each do |invalid_email|
        @user.email=invalid_email
        should_not be_valid
      end
    end

    it "is duplicate in DB" do
      user_duplicate=@user.dup

      user_duplicate.should_not be_valid
    end

    it "is duplicate in DB (upcase)" do
      user_duplicate=@user.dup
      user_duplicate.email.upcase!
      
      user_duplicate.should_not be_valid
    end
    
  end

  describe "authentication :" do
    before :each do
      @user.save
    end

    let(:user_in_bbdd){
      user_in_bbdd=User.find_by_email(@user.email)
    }

    describe "valid password" do
      it{ should == user_in_bbdd.authenticate(@user.password)}
    end

    describe "invalid password" do
      let(:invalid_user){
        user_in_bbdd.authenticate("other password")
      }

      it{ should_not == invalid_user}

      it "and authenticate method should return false" do
        invalid_user.should be_false
      end
    end
  end

  describe "session token:" do
    before{ @user.save}

    it{ @user.session_token.should_not be_blank}
  end

end
