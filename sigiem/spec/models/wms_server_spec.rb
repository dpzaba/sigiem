require 'spec_helper'

describe WmsServer do
  before do
    @wms_server=WmsServer.new(
                              name: "Pantanos",
                              url: "http://dpzaba.com/wms?getMap,etc.."
                              )
  end

  subject{ @wms_server}

  it{ should be_valid}
  
  describe "should respond to" do
    it{ should respond_to :name}
    it{ should respond_to :url}
  end

  describe "url: " do
    it "is too short" do
      @wms_server.url="a"*4
      should_not be_valid
    end
    
    it "is duplicate in DB" do
      server_duplicate=@wms_server.dup
      server_duplicate.save

      should_not be_valid
    end

    it "isn't duplicate in DB (case sensitive)" do
      server_duplicate=@wms_server.dup
      server_duplicate.url=server_duplicate.url.upcase
      server_duplicate.save

      should be_valid
    end

  end

  
  describe "name:" do
    it "is too long" do
      @wms_server.name="a"*201
      should_not be_valid
    end

    it "is too short" do
      @wms_server.name="a"
      should_not be_valid
    end

    it "is empty" do
      @wms_server.name=""
      should_not be_valid
    end

    it "is blank" do
      @wms_server.name=" "
      should_not be_valid
    end
  end
  
end
