require 'spec_helper'

describe Sensor do

  before do
    @sensor=FactoryGirl.build(:sensor)
    device=FactoryGirl.build(:device)
    device.sensors << @sensor
    @sensor.device=device
  end

  subject{ @sensor}
  
  it{ should be_valid}

  it "saving it should create his associated objects (check factories are ok)" do
    @sensor.save
    Sensor.count.should == 1
  end
  
  describe "should respond to" do
    it { should respond_to :name}
    it { should respond_to :type}
    it { should respond_to :values}
    
    it { should respond_to :device}
    it { should respond_to :alert}
  end

  it "should not have a nil device" do
    @sensor.device.should_not == nil
  end
  
  it "should not have an empty name" do
    @sensor.name=""
    should_not be_valid
  end

  it "should not have an empty type" do
    @sensor.name="".to_sym
    should_not be_valid
  end

  it "values should be a flatten array" do
    @sensor.values=[1,3,4,[5,[6,7],8]]
    @sensor.save

    is_flat=true
    
    @sensor.values.each do |value|
      if value.is_a? Array
        is_flat= false
        return
      end
    end

    is_flat.should be_true
  end
    
end


