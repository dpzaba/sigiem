require 'spec_helper'

describe Datum do
  
  before do
    @datum=FactoryGirl.create(:datum)
  end

  subject{ @datum}

  it{ should be_valid}

  describe "should respond to" do
    it{ should respond_to :geojson}
    it{ should respond_to :name}
    it{ should respond_to :category}
  end
  
  describe "should have a valid GeoJSON format" do

    it "invalid type" do
      @datum.geojson["type"]="Invalid_type"

      should_not be_valid
    end

    it "nil type" do
      @datum.geojson["type"]=nil

      should_not be_valid
    end
    
  end

  describe "should have a valid category associated (belongs_to)" do

    it "nil category" do
      @datum.category=nil
      should_not be_valid
    end

    it{ @datum.category.should be_valid}

    it "can't be two data with the same category" do
      other_datum=@datum.dup

      other_datum.should_not be_valid
    end
    
  end

  
end
