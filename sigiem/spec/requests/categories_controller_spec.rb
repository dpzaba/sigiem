#coding: utf-8
require 'spec_helper'

describe "Categories" do

  subject{ page}
  
  describe "Permissions" do

    let(:categories_pages) do
      [
       categories_path,
       new_category_path,
       edit_category_path(FactoryGirl.create(:category))
      ]
    end
    
    it "as user can't access" do
      user=FactoryGirl.create(:user)
      sign_in_test user

      categories_pages.each do |page|
        visit page
        current_path.should == root_path
      end
      
    end

    it "as admin can access" do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin

      categories_pages.each do |page|
        visit page
        current_path.should == page
      end
    end

    describe "ajax path must be access for everyone (POST)" do

      before{ visit root_path}

      it "with a valid category" do
        datum=FactoryGirl.create(:datum)
        
        xhr :post, data_category_path(datum.category.id)

        #response.should be_success
        response.code.should == "200"
      end

     it "with an empty category" do
        category=FactoryGirl.create(:category)
        
        xhr :post, data_category_path(category.id)

        response.code.should == "204"
      end

      it "with an invalid category" do
        category=FactoryGirl.create(:category)
        Category.find_by_name(category.name).destroy
        
        xhr :post, data_category_path(category.id)

        response.code.should == "204"
     end
      
    end
    
  end

  
  describe "Index:" do
      
    let(:number_of_categories){20}
    
    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      
      number_of_categories.times{ FactoryGirl.create(:category)}
      visit categories_path
    end

    it{ current_path.should == categories_path}
    it{ should have_selector 'title',text:"Categorías"}
    it{ should have_selector 'h1', text: "Listado de categorías" }

    it { should have_selector('div.pagination') }

    describe "should show all categories" do
      
      let(:categories) do
        Category.sort(:name).paginate(page: 1)
      end
      
      it "check name" do
        categories.each do |category|
          should have_content category.name.capitalize
          click_link "Eliminar categoría"
        end
      end

      it "check parent" do
        categories.each do |category|
          should have_content category.parent
          click_link "Eliminar categoría"
        end
      end

    end
    
    it "delete all categories" do
      number_of_categories.times do
        click_link "Eliminar categoría"
      end

      Category.count.should == 0
      
    end

    it "whithout categories should show a message" do
      Category.destroy_all
      visit categories_path
      
      should have_content "Todavía no hay ninguna categoría"
    end

    it "link to edit a category" do
      Category.destroy_all
      temporal_category=FactoryGirl.create(:category)
      visit categories_path
      click_link "Modificar datos"
      current_path.should == edit_category_path(temporal_category)
    end

    it "flash" do
      click_link "Eliminar categoría"
      should have_selector('div.alert.alert-success', text: "La categoría y todos sus datos asociados han sido borrados correctamente.")
    end

    
    it "link to add new servers" do
      should have_link "Añadir categoría"
    end
    
  end
  
  describe "Edit:" do

    let(:update_category){ "Actualizar datos"}
    
    let(:test_category){
      FactoryGirl.create(:category)
    }

    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin

      visit edit_category_path(test_category)
    end

    it{ should have_selector 'title',text:"Editar categoría"}
    it{ should have_selector 'h1', text:"Modifica la categoría"}

    describe "should have a form" do
      it{ should have_selector 'form'}
      it{ should have_selector 'label', text: "Nombre"}
      it{ should have_selector 'label', text: "Categoría a la que pertenece"}
      it{ should have_selector 'input', type: 'submit', value: :update_category}
    end

    it "should back to index page" do
      click_link "Cancelar"
      current_path.should == categories_path
    end

    describe "with valid information" do
      let(:new_name){ "Other category"}
      let(:new_parent){
        FactoryGirl.create(:wmsServer)
        Category.last.parent
      }

      before do
        fill_in "Nombre", with: new_name
        fill_in "Categoría a la que pertenece", with: new_parent
        click_button :update_category
      end

      it{ should have_content new_name}
      it{ should have_content new_parent}

      it "should appear a success flash message" do
        should have_selector('div.alert.alert-success',
                                 text: "Categoría actualizada correctamente.")
      end

      it{ current_path.should == categories_path}
      
    end #valid information

    describe "with invalid information" do

      before do
        fill_in "Nombre", with: ""
        fill_in "Categoría a la que pertenece", with: "Don't exist"
        click_button :update_category
      end
      
      it "should appear an error flash message" do
        should have_selector('div.alert.alert-error')
      end

      it "should render edit" do
        current_path.should == category_path(test_category)
      end
      
    end
    
  end #edit
  
  describe "New:" do

    let(:add_category){ "Añadir categoría"}
    
    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin

      visit new_category_path
    end

    it{ should have_selector 'title', text: add_category}
    it{ should have_selector 'h1', text: "Añade una categoría"}

    describe "should have a form" do
      it{ should have_selector 'form'}
      it{ should have_selector 'label', text: "Nombre"}
      it{ should have_selector 'label', text: "Categoría a la que pertenece"}
      it{ should have_selector 'input', type: 'submit', value: :add_category}
    end

    it "should back to index page" do
      click_link "Cancelar"
      current_path.should == categories_path
    end
    
    describe "add category with valid information" do
      let(:new_name){ "New category"}
      let(:new_parent) do
        FactoryGirl.create(:category)
        Category.last.parent
      end
      
      before do
        fill_in "Nombre", with: new_name
        fill_in "Categoría a la que pertenece", with: new_parent
      end

      it "after add a category should show all categories" do
        click_button :add_category
        current_path.should == categories_path
      end
      
      it "should appear the name of the new category" do
        click_button :add_category
        should have_content new_name
      end
    
      it "should appear the parent of the new category" do
        click_button :add_category
        should have_content new_parent
      end
      
      it "should create a new category" do
        number_of_categories=Category.count
        click_button :add_category
        Category.count.should == number_of_categories+1
      end

      it "should show a message" do
        click_button :add_category
        should have_selector('div.alert.alert-success',
                               text: "Categoría creada correctamente.")
      end
    
    end

    describe "add category with invalid information" do

      let(:new_name){ ""}
      let(:new_parent) do
        FactoryGirl.create(:category)
        Category.last.parent
      end
      
      before do
        fill_in "Nombre", with: new_name
        fill_in "Categoría a la que pertenece", with: new_parent
      end

      it "should render the same web" do
        click_button :add_category
        current_path.should == categories_path
      end
      
      it "should not create a new category" do
        number_of_categories=Category.count
        click_button :add_category
        Category.count.should == number_of_categories
      end

      it "should show a message" do
        click_button :add_category
        should have_selector('div.alert.alert-error')
      end

    end #invalid information

  end #new
  
end
