#coding: utf-8
require 'spec_helper'

describe "Main pages" do

  subject{ page}

  describe "About page" do

    before do
      visit acerca_path
    end
    
    it "should have title" do
      should have_selector('title',text: "Acerca de")
    end

    it "should have authors" do
      authors=%w[Alarcos Universidad de Castilla La Mancha INNOCAMPUS David Antonio Pérez Zaba]

      authors.each do |author|
        should have_content author
      end
    end

    it "should show a contact" do
      should have_content "escríbenos a:"
    end

    it{ should have_link "Quiero probarlo"}
    
    it "should have a link to root path" do
      click_link 'Quiero probarlo'
      current_path.should == root_path
    end
    
  end #about page

  describe "Home page" do

    before do
      visit root_path
    end
    
    it "should have title" do
      should have_selector('title',text: "Inicio")
    end

    #map
    it{ should have_selector '.sigiem-map', count: 1 }
    
    #logo SIGIEM
    it{ should have_selector '.logo'}

    #logo UCLM
    it{ should have_selector '.logo_uclm'}
        
    #menu
    describe "should show a accordion menu" do

      let(:categories){ []}
      before do

        5.times do
          categories << FactoryGirl.create(:category)
        end
        
        visit root_path
      end

      it{should have_selector '.menu-button' }

      it{ should have_selector '.accordion'}
      it{ should have_selector '.accordion-group'}
      it{ should have_selector '.accordion-heading'}
      it{ should have_selector '.accordion-toggle'}

      it{ should have_selector '.collapse'}
      it{ should have_selector '.checkbox'}
      it{ should have_selector '.menu-elements'}

      it "should appear its name for each category" do
        categories.each do |category|
          should have_content category.name.capitalize
        end
      end
      
    end
    
    #searchbox
    it{ should have_selector '.searchbox'}
    it{ should have_selector '.searchbox-partial'}

    describe "admin should see" do

      before do
        admin=FactoryGirl.create(:admin)
        sign_in_test admin
        visit root_path
      end
    
      #sensors
      it "Sensors" do
        should have_link "Mostrar sensores"
      end

      #alerts
      it "Alerts" do
        should have_selector('.div_alerts')
      end
      
    end
  end #home page
  
end
