#coding: utf-8
require 'spec_helper'

describe "Record Alerts" do

  subject{ page}

  describe "Access permissions" do
    before{ visit record_alerts_path}

    it{ current_path.should == root_path}

    it "ordinary users can't access" do
      user=FactoryGirl.create(:user)
      sign_in_test user
      visit record_alerts_path

      current_path.should == root_path
    end

    it "as admin can access" do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      visit record_alerts_path
      
      current_path.should == record_alerts_path
    end
  end #access

  
  describe "Index: " do

    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      
      visit record_alerts_path
    end
    

    it{ should have_selector 'title', text: "Alertas"}
    it{ should have_selector 'h1', text: "Registro de alertas"}

    describe "should show each record of alerts of each alert" do
      
    end
    
    describe "deleting all alerts" do
      
    end
    
  end
  
end
