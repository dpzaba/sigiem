#coding: utf-8
require 'spec_helper'

describe "Emulator" do

  subject{ page}

  describe "Access permissions" do

    before do
      visit emulator_path
    end

    it{ current_path.should == root_path}

    it "ordinary users can't access" do
      user=FactoryGirl.create(:user)
      sign_in_test user
      visit emulator_path

      current_path.should == root_path
    end

    it "as admin can access" do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      visit emulator_path
      
      current_path.should == emulator_path
    end
  end #access

  
  describe "New" do

    before do
      Device.remove
      Sensor.remove
      Alert.remove

      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      visit emulator_path
    end

    describe "with new valid data" do

      before do
        fill_in "Nombre del dispositivo", with: "Test device"
        fill_in "Nombre del sensor", with: "Test sensor"
        click_on "Generar datos"
      end
      

      it "should render the sending page" do
        current_path.should == emulators_path
      end
    
      it "should send and create new sensor data" do
        click_on "Enviar datos"
      
        Device.count.should == 1
        Sensor.count.should ==1
        Alert.count.should ==1
      end

    end

    describe "with invalid names" do

      before do
        fill_in "Nombre del dispositivo", with: ""
        fill_in "Nombre del sensor", with: ""
        click_on "Generar datos"
      end

      it "should render the same page" do
        current_path.should == emulator_path
      end
      
      it "should show an error message" do
        should have_selector('div.alert.alert-error', text: "Alguno de los nombres no es válido")
      end
      
    end

    
  end #new

  describe "Sending data" do
    
    before do
      admin=FactoryGirl.create(:admin)
      sign_in_test admin
      visit emulator_path
    end
    
    describe "adding data to device" do

      before do
        @device=FactoryGirl.build(:device)
        sensor=FactoryGirl.build(:sensor)
        sensor.device=@device
        @device.sensors << sensor
        @device.save
        sensor.save
      end

      it "should add new data values to the sensor (that exists before)" do
        number_of_values_before=@device.sensors.first.values.length
        number_of_sensors_before=@device.sensors.length

        fill_in "Nombre del dispositivo", with: @device.name
        fill_in "Nombre del sensor", with: @device.sensors.first.name
        
        click_on "Generar datos"
        click_on "Enviar datos"

        @device.reload
        @device.sensors.length.should == number_of_sensors_before
        @device.sensors.first.values.length.should >= number_of_values_before
      end
      
      it "should add new sensor (and his data) to the device" do
        number_of_sensors_before=@device.sensors.length
        
        fill_in "Nombre del dispositivo", with: @device.name
        fill_in "Nombre del sensor", with: @device.sensors.first.name+"_new"
        
        click_on "Generar datos"
        click_on "Enviar datos"
        
        @device.reload
        @device.sensors.length.should == number_of_sensors_before +1
      end
      
    end
    
  end # sending data

  describe "Ajax" do

    before do
      Alert.remove
      Sensor.remove
      Device.remove
      
      @alert=FactoryGirl.build(:alert)
      @device=FactoryGirl.build(:device)
      @sensor=FactoryGirl.build(:sensor)
      @sensor.device=@device
      @device.sensors << @sensor #add to the DB
      @sensor.alert=@alert
      @alert.sensor=@sensor
    end
    
    
    it "if device and sensor exists just add data to that sensor" do
      @device.save
      @sensor.save
      @alert.save

      length_values_before=@sensor.values.length
      
      data={
        device_name: @device.name, device_lat: @device.lat, device_long: @device.long,
        sensor_name: @sensor.name, sensor_type: @sensor.type, sensor_values: @sensor.values,
        alert_limit_sup: @alert.limit_sup, alert_limit_inf: @alert.limit_inf
      }

      xhr :post, sensors_data_path(data)

      response.code.should == "200"#be_success
      Alert.count.should == 1
      Sensor.count.should == 1
      Device.count.should == 1

      @sensor.reload
      @sensor.values.length.should == length_values_before*2

      
      Device.first.sensors.first.should == Sensor.first
      Sensor.first.alert.should == Alert.first
      Alert.first.sensor.should == Sensor.first
    end
    
    
    it "with valid data should create a new device, new sensor and a new alert" do
      @sensor.destroy #remove (it was added with @device.sensors << @sensor)
      
      data={
        device_name: @device.name, device_lat: @device.lat, device_long: @device.long,
        sensor_name: @sensor.name, sensor_type: @sensor.type, sensor_values: @sensor.values,
        alert_limit_sup: @alert.limit_sup, alert_limit_inf: @alert.limit_inf
      }

      xhr :post, sensors_data_path(data)

      response.code.should == "200"#be_success
      Alert.count.should == 1
      Sensor.count.should == 1
      Device.count.should == 1

      Device.first.sensors.first.should == Sensor.first
      Sensor.first.alert.should == Alert.first
      Alert.first.sensor.should == Sensor.first
    end

    it "if device exists should create a new sensor and an alert" do
      @device.sensors << @sensor
      @device.save
      @alert.save
      
      @sensor.name=@sensor.name+"_new"
            
      data={
        device_name: @device.name, device_lat: @device.lat, device_long: @device.long,
        sensor_name: @sensor.name, sensor_type: @sensor.type, sensor_values: @sensor.values,
        alert_limit_sup: @alert.limit_sup, alert_limit_inf: @alert.limit_inf
      }

      xhr :post, sensors_data_path(data)

      response.code.should == "200"#be_success
      Alert.count.should == 2
      Sensor.count.should == 2
      Device.count.should == 1

      Device.first.sensors.first.should == Sensor.first
      Sensor.first.alert.should == Alert.first
      Alert.first.sensor.should == Sensor.first
    end

  end
end
