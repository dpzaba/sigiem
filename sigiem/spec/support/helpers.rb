#coding: utf-8
def sign_in_test(user)
  visit entrar_path

  fill_in "Email", with: user.email
  fill_in "Contraseña", with: user.password

  click_button "Entrar"
  #cookies[:session_token]=user.session_token
end

def links_for_signed_in_users
  it{ should_not have_link 'Iniciar sesión'}
  it{ should have_link 'Salir'}
end

def content_for_signed_in_users(user)
  should have_content user.name
end

def create_device_with_sensors
  device=FactoryGirl.build(:device)
  2.times do
    sensor=FactoryGirl.build(:sensor)
    alert=FactoryGirl.build(:alert)

    sensor.device=device
    device.sensors << sensor

    sensor.alert=alert
    alert.sensor=sensor
    
    alert.save
    sensor.save
  end
  device.save
  return device
end
