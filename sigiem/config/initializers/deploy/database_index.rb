User.ensure_index(:email, unique: true, :case_sensitive => false)
User.ensure_index(:session_token)

Category.ensure_index(:name, unique: true)
Category.ensure_index(:parent)

Datum.ensure_index(:category_id)

Device.ensure_index(:name)

Sensor.ensure_index(:device)
Sensor.ensure_index(:alert)

Alert.ensure_index(:sensor)
Alert.ensure_index(:record)
