ENV['RAILS_ENV'] = 'development'

def read_geojson file
  text=""
  File.open(file) do |f|
    text=f.read
  end
  text=JSON.parse(text)
  return text
end


User.remove
Category.remove
Datum.remove
Device.remove
Sensor.remove
Alert.remove


puts "-"*50
puts "Loading example data"
puts "-"*50


#Users
puts "Users"
User.create(
        email: "user@user.com",
        name: "David Perez",
        password: "useruser",
        password_confirmation: "useruser")

admin=User.create(
              email: "admin@admin.com",
              name: "David Perez",
              password: "adminadmin",
              password_confirmation: "adminadmin")
admin.role= :admin
admin.password="adminadmin"
admin.password_confirmation="adminadmin"
admin.save

super_admin=User.create(
                    email: "super_admin@super_admin.com",
                    name: "David Perez",
                    password: "super_adminsuper_admin",
                    password_confirmation: "super_adminsuper_admin")
super_admin.role= :super_admin
super_admin.password="super_adminsuper_admin"
super_admin.password_confirmation="super_adminsuper_admin"
super_admin.save
  
  
#Data
puts "Data"

def search_files path, parent
  FileUtils.cd path
  
  Dir.glob("*").select do |item|
    new_item=File.join path,item
    
    if File.directory? item
      Category.create(name: item, parent: parent)
      #puts "crea categoria #{item} en #{parent}"
      new_item=File.join item

      search_files new_item, item
    else
      if File.file? item
        geojson=read_geojson item
        category=Category.find_by_name(parent)
        datum=Datum.new(name: item, geojson: geojson)
        datum.category=category
        datum.save
        #puts datum.errors.messages
      end
    end
  end

  FileUtils.cd ".."
end


search_files "data_deploy",""

  
#Devices, sensors and alerts
puts "Devices, sensors and alerts"

(1..3).to_a.each do |n|
    device=Device.new(name: "device #{n}",
                 long: Random.new.rand(-4.5..-2), 
                 lat: Random.new.rand(38.78..40.8))

    (1..1).to_a.each do |s|
      sensor=Sensor.new(name: "sensor #{s}", type: "#{s}",values: [])
      
      alert=Alert.new(limit_sup: 50, limit_inf: 10)
      alert.sensor=sensor
      sensor.alert=alert
      
      sensor.device=device
      device.sensors << sensor
      
      alert.save
      sensor.save
      
    end

    device.save
end

puts "-"*50


