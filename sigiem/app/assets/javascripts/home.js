// 
$(function(){
    show_categories_menu();

    $('.menu_checkbox').change(function(){

	var checkbox_id=(this.id).toString();

	if(this.checked){
	    add_layer_data(checkbox_id);
    	}else{
	    remove_layer_data(checkbox_id);
	}
	
    	select_all_subcategories(this.name, this.checked);
    
    });

    $('#toggle_checkboxs').click(function(){
	toggle_all_checkboxs();
    });

    $('#sensors_data').click(function(){
	toggle_sensors();
    });

});


// Categories menu
var state_unselected="Visualizar todos los Puntos de Interés";
var state_selected="Ocultar todos los Puntos de Interés";


function show_categories_menu(){
    $("#toggle_checkboxs").text(state_unselected);
    $(".menu-button").css("visibility", "visible");
}

function toggle_all_checkboxs() {
    var element=$("#toggle_checkboxs");

    if(element.text()===state_unselected){
	element.text(state_selected);
	var status=true;
    }else{
	element.text(state_unselected);
	var status=false;
    }

    $(".menu_checkbox.menu-accordion").each(function(){
	$(this).attr("checked",status);
	$(this).change();
    });
}

function select_all_subcategories(name, status){
    var class_name=name.substring(1,name.length)
    var sub_categories=$(".menu_checkbox."+class_name)

    sub_categories.each(
    	function(){
    	    $(this).attr("checked",status)
	    $(this).change();
    	}
    )
}

function toggle_sensors(){
    element=document.getElementById("sensors_data");
    var text_hide="Ocultar sensores";
    var text_show="Mostrar sensores";

    if(element.text == text_hide){
	element.text = text_show;
	remove_layer_data("sensors");
    }else{
	element.text = text_hide;
	add_layer_data("sensors");
    }
}
