var map=null;
var layer_cache={};

window.onload=function(){
    if($("#div_map").length){
	map = L.map('div_map',
    		    {
			//crs: L.CRS.EPSG4326,
    			center: [39.474, -2.889],
    			zoom: 6
    		    }
    		   )

	L.tileLayer(
    	    'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
    	    {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    		maxZoom: 18
    	    }
	).addTo(map);

	function onMapClick(e) {
    	    console.log("You clicked the map at " + e.latlng.toString())
	}

	map.on('click', onMapClick);
    }
};


function resize_map() {
    map_element=document.getElementById('div_map');
    height=window.innerHeight-235;
    map_element.style.height=height+"px";
}

function add_layer_data(id_data){

    if(!layer_cache[id_data]){
    	$("#"+id_data+"_link").click();
    }else{
	map.addLayer(layer_cache[id_data]);
    }
}

function remove_layer_data(id_data){

    layer=layer_cache[id_data];

    if(layer){
	map.removeLayer(layer);
    }
}


function add_popup_info(feature, layer){
    
    var popup_content="";

    if(!feature.properties || $.isEmptyObject(feature.properties)){
	return;
    }

    if(feature.properties.popupContent){
	popup_content=feature.properties.popupContent;
    }else{

	for(var propertie in feature.properties){
	    if(feature.properties.hasOwnProperty(propertie)){
		popup_content+="<b>"+propertie+": </b>"+feature.properties[propertie]+"</br>";
	    }
	}

    }
    
    layer.bindPopup(popup_content);
    
}

function add_style(feature){
    var style="";
    
    if(!feature.properties){
	return;
    }
    
    if(feature.properties.style){
	style=feature.properties.style;
    }else{
	style=add_random_color();
    }

    return style;
}

function add_random_color(){
    return {color: '#'+(Math.random()*0xFFFFFF<<0).toString(16) }
}

