class WmsServer
  include MongoMapper::Document
  safe
  
  attr_accessible :name, :url

  key :name, String, required: true, length: 5..50, :case_sensitive => false
  key :url, String, required: true, unique: true, case_sensitive: true

  validates_length_of :url, minimum: 5
  #regex_url?
  #category?
  
end
