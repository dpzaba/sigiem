class Category
  include MongoMapper::Document
  safe

  attr_accessible :name, :parent

  key :name, String, required: true, length: 0..30
  key :parent, String, required: false, length: 0..30, default: "", :case_sensitive => false

  validates_uniqueness_of :name, :case_sensitive => false

  before_save :downcase_properties
  after_save :add_new_parent_as_category
  after_update :add_new_parent_as_category

  one :datum

  
  private

  def downcase_properties
    self.name.downcase!
    self.parent.downcase!
  end
  
  def add_new_parent_as_category
    if not Category.distinct(:name).include? self.parent
      Category.create(name: self.parent, parent: "")
    end
  end
  
end
