class Sensor
  include MongoMapper::Document
  safe

  attr_accessible :name, :type, :values, :alert, :device

  key :name, String, required: true
  key :type, Symbol, required: true

  key :values, Array

  one :alert, :dependent => :destroy
  belongs_to :device

  before_save :flatten_values
  validates_presence_of :device
  
  private
  
  def flatten_values
    self.values.flatten!
  end

end
