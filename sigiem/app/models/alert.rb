#coding: utf-8
class Alert
  include MongoMapper::Document
  safe

  attr_accessible :limit_sup, :limit_inf, :sensor

  key :limit_sup, Float, default: nil, allow_nil: true
  key :limit_inf, Float, default: nil, allow_nil: true
  key :record, Array, default: []
  key :index_last_value, Integer, default: 0
  
  one :sensor
  
  before_save :swap_limits
  validate :format_limits
  validates_presence_of :sensor


  def check_values_of_sensor
    number_of_values=self.sensor.values.length
    values_to_check=self.sensor.values[self.index_last_value...number_of_values]

    values_to_check.each do |value|
      if value < self.limit_inf or value > self.limit_sup
        self.record << {value: value, time: Time.now}
      end
    end

    self.index_last_value=number_of_values
  end

  private
  
  def format_limits
    if self.limit_sup.nil? and self.limit_inf.nil?
      errors.add(:format_limits, "debe tener almenos un límite superior o inferior")
    end
  end
  
  def swap_limits
    if not self.limit_sup.nil? and not self.limit_inf.nil?
      if self.limit_sup < self.limit_inf
        #swap values
        self.limit_sup, self.limit_inf = self.limit_inf, self.limit_sup
    end

    end
  end
  
end
