class User
  include MongoMapper::Document
  safe

  attr_accessible :name, :email, :password, :password_confirmation

  include ActiveModel::SecurePassword
  has_secure_password

  #rfc?
  #http://ruby.railstutorial.org/chapters/modeling-users#code:validates_format_of_email
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  key :name, String, required: true, length: 3..30
  key :email, String, required: true, format: VALID_EMAIL_REGEX
  key :role, Symbol, required: true, in: [:user, :admin, :super_admin], default: :user

  key :session_token, String
  key :password_digest, String, required: true

  validates_uniqueness_of :email, :case_sensitive => false

  validates_presence_of :password
  validates_presence_of :password_confirmation
  validates_length_of  :password, minimum: 6
 
  before_save :create_session_token
  before_save :downcase_email
  
  private
  
  def create_session_token
    self.session_token = SecureRandom.urlsafe_base64
  end

  def downcase_email
    self.email.downcase!
  end
  
end
