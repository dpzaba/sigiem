
class WmsServersController < ApplicationController

  before_filter :signed_in
  before_filter :signed_in_as_admin
  
  def index
    @wms_servers=WmsServer.sort(:name).paginate(page: params[:page], per_page: 10)
  end

  def new
    @server=WmsServer.new
  end

  def create
    @server=WmsServer.new(params[:wms_server])
    if @server.save
      flash[:success]="Servidor creado correctamente."
      redirect_to wms_servers_path
    else
      render "new"
    end
  end

  def edit
    @server=WmsServer.find_by_id(params[:id])
  end

  def update
    @server=WmsServer.find_by_id(params[:id])
    
    if @server.update_attributes(params[:wms_server])
      flash[:success]= "Se han actualizado los datos correctamente."
      redirect_to wms_servers_path
    else
      render "edit"
    end
    
  end

  def destroy
    @server=WmsServer.find_by_id(params[:id])
    
    if @server.destroy
      flash[:success]= "Servidor borrado correctamente."
    else
      flash[:error]= "No se ha podido borrar ese servidor."
    end
    
    redirect_to wms_servers_path
  end
  
end
