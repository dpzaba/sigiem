#coding: utf-8
class UsersController < ApplicationController
  
  before_filter :signed_in, only: [:edit, :destroy, :update, :index, :change_role]
  before_filter :allowed_user, only: [:edit, :update]
  before_filter :signed_in_as_admin, only: [:index, :change_role]
  
  def new
    if not signed_in?
      @user=User.new
    else
      redirect_to root_path, notice: "Usted ya está registrado, si quiere crear otra cuenta salga de esta primero."
    end
  end

  def edit
    # @user=User.find_by_id(params[:id]) => before_filter allowed_user
  end

  def destroy
    User.find_by_id(params[:id]).destroy
    flash[:success]="Usuario eliminado correctamente."
    redirect_to users_path
  end

  def create
    @user=User.new(params[:user])
    if @user.save
      sign_in @user
      flash[:success]="Bienvenido, se ha registrado correctamente."
      redirect_to root_path
    else
      render "new"
    end
  end

  def update
    # @user=User.find_by_id(params[:id]) => before_filter allowed_user
    if @user.update_attributes(params[:user])
      flash.now[:success]="Datos actualizados correctamente."
      sign_in @user
    end

    render 'edit'
  end

  def index
    @users=User.sort(:email).paginate(page: params[:page], per_page: 10)
  end

  def change_role
    @user=User.find_by_id(params[:id])
    role_in_spanish=spanish_role(inverse_role(@user.role))
    
    if @user.set(role: inverse_role(@user.role))
      flash[:success]="Se ha cambiado el rol de #{@user.name} a #{role_in_spanish} correctamente."
    else
      flash[:error]="Se ha producido un error al intentar cambiar el rol de #{@user.name} a #{role_in_spanish}."
    end
    redirect_to users_path
  end

  
  private

  def allowed_user
    @user=User.find_by_id(params[:id])
    unless @user==current_user
      redirect_to root_path, notice: "No puedes acceder a esos datos o realizar esa acción (el acceso a otros usuarios no está permitido)."
    end
  end

end
