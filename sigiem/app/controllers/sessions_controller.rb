#coding: utf-8
class SessionsController < ApplicationController

  def new
    if signed_in?
      redirect_to root_path, notice: "Ya has iniciado sesión, si quieres entrar con otra cuenta sal de esta primero."
    end
  end

  def create
    user=User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      flash[:success] = "Hola #{user.name}, bienvenido."
      sign_in user
      redirect_to root_path
    else
      flash[:error] = 'Email o contraseña incorrectos.'
      redirect_to entrar_path
    end
  end
  
  def destroy
    sign_out
    redirect_to root_path, :notice => "Has salido correctamente, hasta pronto."
  end
end
