class DevicesController < ApplicationController

  before_filter :signed_in, except: [:sensors_data]
  before_filter :signed_in_as_admin, except: [:sensors_data]

  
  def edit
    @device=Device.find_by_id(params[:id])
  end

  def update
    updated_correct=true
    @device=Device.find_by_id(params[:id])
    
    @device.sensors.each_index do |i|
      params_alert=params[:device][:sensors][i][:alert]
      sensor=@device.sensors[i]
      alert=sensor.alert
      
      if alert.nil?
        alert=Alert.new(params_alert)
        alert.sensor=sensor
        sensor.alert=alert
        updated_correct=alert.save
      else
        updated_correct=alert.update_attributes(params_alert)
      end

      if not updated_correct
        break
      end
    end #each sensor

    if updated_correct
      redirect_to devices_path
    else
      render "edit"
    end
    
  end
  

  def destroy
    @device=Device.find_by_id(params[:id])
    
    if @device.destroy
      flash[:success]="El dispositivo #{@device.name} se ha borrado completamente"
    else
      flash[:error]="Error al borrar el dispositivo."
    end
    redirect_to devices_path
  end

  def index
    @devices=Device.all
        
    respond_to do |f|
      f.js
      f.html
    end
  end

  
  def sensors_data
    
    if params[:sensor_values].nil?
      params[:sensor_values]=[]
    end

    params[:device_name].downcase!
    params[:sensor_name].downcase!
    params[:sensor_values].collect! { |string| string.to_i}

    @device=Device.find_by_name(params[:device_name])
    alert=Alert.new( limit_sup: params[:alert_limit_sup], limit_inf: params[:alert_limit_inf])
    
    #don't exists the device
    if @device.nil?

      @device=Device.new(name: params[:device_name], lat: params[:device_lat], long: params[:device_long])
      sensor=Sensor.new(name: params[:sensor_name], type: params[:sensor_type], values: params[:sensor_values], device: @device)
      sensor.alert=alert
      alert.sensor=sensor
      @device.sensors << sensor
      
    else

      @device.lat=params[:device_lat]
      @device.long=params[:device_long]

      sensor=@device.sensors.find_by_name(params[:sensor_name])
      #exists the device but don't exists that sensor
      if sensor.nil?

        sensor=Sensor.new(name: params[:sensor_name], type: params[:sensor_type], values: params[:sensor_values], device: @device)
        alert.sensor=sensor
        sensor.alert=alert
        @device.sensors << sensor
      else
        #exists the device and the sensor
        sensor.values << params[:sensor_values]

        if sensor.alert.nil?
          sensor.alert=alert
        else
          alert=sensor.alert
        end

        sensor.save
      end

    end

    alert.sensor=sensor
    @device.save
    if not alert.nil?
      alert.check_values_of_sensor
      alert.save
    end
    
    respond_to do |format|
      format.js
    end

  end
  
end
