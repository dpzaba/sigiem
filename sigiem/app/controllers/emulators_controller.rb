#coding: utf-8
class EmulatorsController < ApplicationController

  before_filter :signed_in
  before_filter :signed_in_as_admin
  
  def new
  end
  
  def create
    random_numbers=Random.new
    
    #device
    lat=random_numbers.rand(38.5901..40.372).round(5)
    long=random_numbers.rand(-5.24..-1.57).round(5)

    @device=Device.new(name: params[:name], lat: lat, long: long)
    
    #alert
    d=Device.find_by_name(params[:name])
    if d
      s=d.sensors.find_by_name(params[:name_sensor])
    end
    @alert=nil
    if s and not s.alert.nil?
      @alert=s.alert
    else
      @alert=Alert.new(limit_sup: random_numbers.rand(51..100),
                       limit_inf: random_numbers.rand(1..50))
    end

    #sensor
    @sensor=Sensor.new(name: params[:name_sensor], type: "Temperatura")
    @sensor.values=(
                   random_numbers.rand(-10..@alert.limit_sup).round..
                   random_numbers.rand(@alert.limit_inf..110).round
                   ).step(5).to_a
    
    if params[:name].empty? or params[:name_sensor].empty?
      flash[:error]="Alguno de los nombres no es válido."
      redirect_to emulator_path
    else
      render "sending_data"
    end

  end
  
  
end
