#coding: utf-8
class CategoriesController < ApplicationController

  before_filter :signed_in, except: [:data]
  before_filter :signed_in_as_admin, except: [:data]

  def index
    @categories=Category.sort(:name).paginate(page: params[:page], per_page: 10)
  end

  def new
    @category=Category.new
  end

  def create
    @category=Category.new(params[:category])

    if @category.save
      flash[:success]="Categoría creada correctamente."
      redirect_to categories_path
    else
      render 'new'
    end
    
  end

  def edit
    @category=Category.find_by_id(params[:id])
  end

  def update
    
    @category=Category.find_by_id(params[:id])

    if @category.update_attributes(params[:category])
      flash[:success]="Categoría actualizada correctamente."
      redirect_to categories_path
    else
      flash[:error]="No se ha podido actualizar los datos."
      render "edit"
    end
    
  end

  def destroy
    category=Category.find(params[:id])
    if category.destroy

      if category.datum
        category.datum.destroy
      end
      
      flash[:success]= "La categoría y todos sus datos asociados han sido borrados correctamente."
    else
      flash[:error]= "No se ha podido borrar la categoría."
    end
    
    redirect_to categories_path
  end

  
  def data

    category=Category.find_by_id(params[:id])
    if not category.nil?
      data=Datum.find_by_category_id(category.id)
    end
    
    respond_to do |format|
      format.js do
        #empty categories (without data) and invalid categories
        if data.nil? 
          render :status => :no_content
        else
          @result= {
            geojson: JSON.generate(data.geojson).html_safe,
            id: category.id.to_s
          }
        end
      end
    end
    
  end

  
end
