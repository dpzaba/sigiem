#coding: utf-8
class RecordAlertsController < ApplicationController

  before_filter :signed_in
  before_filter :signed_in_as_admin

  
  def index
    @record_alerts=Alert.where(:record.ne => []).all
  end

  def info
    @info={}
    @info[:number_of_sensors]=Alert.where(:record.ne => []).count
    @info[:number_of_alerts]=Alert.fields(:record).where(:record.ne => []).all
      .inject(0){ |ac,element| ac+element.record.length }
    @info[:exist]= @info[:number_of_alerts] > 0
    
    render "info_alerts", layout: false
  end

  def destroy
    @alert=Alert.find_by_id(params[:id])
    @alert.record.clear
    
    if @alert.save
      flash[:success]= "Todas las alertas de #{@alert.sensor.name} han sido borradas correctamente."
    else
      flash[:error]= "No se han podido borrar todas las alertas."
    end

    redirect_to record_alerts_path
  end

  def destroy_record

    params[:record]=params[:record].to_i
    
    @record=nil
    alert=Alert.find_by_id(params[:alert])
    
    if not alert.nil?
      @record=alert.record.delete_at(params[:record]).to_s
    end
    
    if not @record.nil? and alert.save
      flash[:success]= "Alerta borrada correctamente."
    else
      flash[:error]= "No se ha podido borrar la alerta."
    end

    redirect_to record_alerts_path
  end
  
  
end
