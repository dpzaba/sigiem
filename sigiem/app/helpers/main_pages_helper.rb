module MainPagesHelper

  def main_categories
    Category.find_all_by_parent("")
  end

  def sub_categories(category)
    Category.find_all_by_parent(category)
  end

  def have_sub_categories?(category)
    not sub_categories(category).empty?
  end
    
end
