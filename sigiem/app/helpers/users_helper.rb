module UsersHelper

  def spanish_role(role)
    role_hash={
      user: "Usuario",
      admin: "Administrador",
      super_admin: "Administrador supremo"
    }
    role_hash[role]
  end

  def inverse_role(role)
    inverse_hash={
      user: :admin,
      admin: :user,
      super_admin: ""
    }
    inverse_hash[role]
  end

end
