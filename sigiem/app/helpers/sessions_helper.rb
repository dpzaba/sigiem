#coding: utf-8
module SessionsHelper

  def sign_in(user)
    cookies.permanent[:session_token]=user.session_token
    self.current_user=user
  end

  def sign_out
    self.current_user=nil
    cookies.delete(:session_token)
  end

  def signed_in?
    !current_user.nil?
  end

  def signed_in
    unless signed_in?
      redirect_to root_path, notice: "Debes iniciar sesión antes."
    end
  end

  #must be allways after signed_in
  def signed_in_as_admin
    unless admin?
      redirect_to root_path, notice: "Debes ser administrador para acceder a este recurso."
    end
  end
  
  #get
  def current_user
    @current_user ||= User.find_by_session_token(cookies[:session_token])
  end
  
  #set
  def current_user=(user)
    @current_user=user
  end

  #must be allways after signed_in
  def admin?
    current_user.role==:admin || current_user.role==:super_admin
  end
  

end

