module CategoriesHelper
  
  def data_source_for_categories
    list=Category.distinct(:name).to_s
  end

  def options_for_categories_text_field(place_holder="Mostrar...", options={})
    default_options={
      :'data-provide'=> "typeahead",
      :'data-source'=> data_source_for_categories,
      placeholder: place_holder,
      :'data-items'=>"6"
    }

    return default_options.merge(options)
  end
  
end
