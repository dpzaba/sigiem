=====================================
`SIGIEM <http://projects.dpzaba.com/sigiem>`_ 
=====================================

Online Geographical Information System with real time data for `Castile-La Mancha <https://en.wikipedia.org/wiki/Castile-La_Mancha>`_ (Spain).

Try it in `sigiem.uclm.es <http://sigiem.uclm.es>`_ (maybe, it isn't the latest version)

Features::

- Add new data to display (it supports a lot of GIS standards, thanks to OGR).
- Receive new data from sensors (API).
- Display alerts of sensors in real time.
- Manage users, data categories and alerts.
- Sensors emulator.
- Script to deploy with data example.

.. image:: https://bitbucket.org/dpzaba/sigiem/raw/master/screenshot_show_all.png

Used tools::

  1-  Ruby (v 1.9.1)
  2-  Rails (v 3.2.9)
  3-  MongoDB (MongoMapper)
  4-  BootStrap 
  5-  Javascript and JQuery (Ajax)
  6-  Leaflet
  7-  Rspec (2.10.0)
  8-  Capybara (1.1.2)
  9-  Git 
  10- Emacs 
  11- GNU/Linux
  12- Apache + Passenger
  13- I18n (spanish translation)
  14- Spork and Guard (continuous integration)
  15- GDAL (Geospatial Data Abstraction Library) and OGR
  16- SimpleCov (code coverage analysis tool for Ruby 1.9)
  17- Maps from OpenStreetMap provided by CloudMade


Some stats (12 Jan 2013)::

-  Code LOC: 849     Test LOC: 1640     Code to Test Ratio: 1:1.9
-  Number of tests: 325
-  Test coverage: 91.11%

`David Zaba <http://www.dpzaba.com>`_
======================================================================================================